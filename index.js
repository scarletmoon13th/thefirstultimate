/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src/App';
import DestinationsDetail from './src/Temp/TravelTab/Destinations_Detail';
import {name as appName} from './app.json';


AppRegistry.registerComponent(appName, () => App);
