import React, { Component } from "react"
import {Platform} from "react-native"
import Routers from './Routers'

export default class App extends Component {
    
    render(){
        return(
            <Routers/>
        )
    }
}