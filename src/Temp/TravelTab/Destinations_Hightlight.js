import React, { Component } from "react"
import {
  View,
  ScrollView,
  TouchableOpacity,
  Text,
  FlatList,
  Image
} from "react-native"
import styles from "../styles/Destinations_High.style"
import { Actions } from "react-native-router-flux";

export default class DestinationsHightlight extends Component {
  constructor(props) {
    super(props)
    this.state = {
      Highlight_D: [{ key: 1 }, { key: 2 }, { key: 3 }]
    }
  }

  
  SeenMore(){
    Actions.Dest()
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={{ alignItems: "center" }}>
          <View style={styles.imageheader}>
          <Image
          source={require("../../assets/images/example/destinations1.jpg")}
          style={styles.image}
           />
            <Text style={[styles.Headertitle]} numberOfLines={2} ellipsizeMode={"tail"}>
              DESTINATIONS
            </Text>
          </View>
          <View style={styles.TarvelStyleTitlebox}>
            <View style={styles.HighlightTitlebox}>
              <View style={styles.Line} />
              <Text style={styles.TarvelStyleTitle}> HIGHLIGHT </Text>
              <Text
                style={[
                  styles.TarvelStyleTitle,
                  { fontFamily: "Kanit-SemiBold", color: "#DF242B" }
                ]}
              >
                DESTINATION
              </Text>
            </View>

            <TouchableOpacity style={styles.morebotton} onPress={()=>{this.SeenMore()}}>
              <Text style={styles.moretext}>ดูทั้งหมด</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={this.state.Highlight_D}
            renderItem={this.HighlightItem}
            contentContainerStyle={{ padding: 10 }}
            // numColumns={4}
          />
        </ScrollView>
      </View>
    )
  }
  HighlightItem = item => {
    return (
      <TouchableOpacity style={styles.HighlightItembox} onPress={()=>{Actions.DestDe()}}>
        <View style={styles.HighlightItemHeaderbox}>
        <Image
        source={require("../../assets/images/example/Forbiden.jpg")}
        style={styles.image}
      />
          <Text style={styles.title} numberOfLines={2} ellipsizeMode={"tail"}>
            Forbidden City, Beijing (China)
          </Text>
        </View>
      </TouchableOpacity>
    )
  }
}
