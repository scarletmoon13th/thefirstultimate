import React, { Component } from "react"
import {
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  Text,
  FlatList
} from "react-native"
import styles from "../styles/TarvelPlanhot.style"

export default class TarvelStoryhot extends Component {
  constructor(props) {
    super(props)
    this.state = {
      Highlight_D: [{ key: 1 }, { key: 2 }, { key: 3 }]
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={{ alignItems: "center" }}>
          <View style={styles.imageheader}>
            <Image
              source={require("../../assets/images/example/destinations1.jpg")}
              style={styles.image}
            />
            <Text
              style={[styles.Headertitle]}
              numberOfLines={2}
              ellipsizeMode={"tail"}
            >
              Tarvel Story
            </Text>
          </View>
          <View style={styles.Topbox}>
            <TouchableOpacity style={styles.leftinnerbox}>
              <Text style={styles.TextLeft}>เข้าสู่ระบบ</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.rightinnerbox}>
              <Text style={styles.TextRight}>สมัครเขียนบล๊อค</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.TarvelStyleTitlebox}>
            <View style={styles.HighlightTitlebox}>
              <View style={styles.Line} />
              <Text style={styles.TarvelStyleTitle}> Hot</Text>
              <Text
                style={[
                  styles.TarvelStyleTitle,
                  { fontFamily: "Kanit-SemiBold", color: "#DF242B" }
                ]}
              >
                Topic
              </Text>
            </View>
          </View>
          <TouchableOpacity style={styles.Hotbox}>
            <View style={styles.HighlightItemHeaderbox}>
              <Image
                source={require("../../assets/images/example/Ski.jpg")}
                style={styles.image}
              />
            </View>
            <View style={styles.HighlightItemDetailbox}>
              <Text
                style={styles.DetailTitle}
                numberOfLines={2}
                ellipsizeMode={"tail"}
              >
                Title
              </Text>
              <Text
                style={[styles.DetailTitle, { color: "#BABABA", marginTop: 0 }]}
              >
                K9 Around The World
              </Text>
              <Text
                style={[styles.DetailTitle, { color: "black", marginTop: 0 }]}
              >
                1234 View
              </Text>
            </View>
          </TouchableOpacity>

          <FlatList
            data={this.state.Highlight_D}
            renderItem={this.HighlightItem}
            horizontal={true}
          />

          <View style={styles.TarvelStyleTitlebox}>
            <View style={styles.HighlightTitlebox}>
              <View style={styles.Line} />
              <Text style={styles.TarvelStyleTitle}> LATEST</Text>
              <Text
                style={[
                  styles.TarvelStyleTitle,
                  { fontFamily: "Kanit-SemiBold", color: "#DF242B" }
                ]}
              >
                POSTS
              </Text>
            </View>

            <TouchableOpacity style={styles.morebotton}>
              <Text style={styles.moretext}>ดูทั้งหมด</Text>
            </TouchableOpacity>
          </View>

          <FlatList
            data={this.state.Highlight_D}
            renderItem={this.latestposts}
            // horizontal={true}
          />
        </ScrollView>
      </View>
    )
  }
  HighlightItem = item => {
    return (
      <TouchableOpacity style={styles.Hotbox_S}>
        <View style={styles.HighlightItemHeaderbox_S}>
          <Image
            source={require("../../assets/images/example/Ski.jpg")}
            style={styles.image}
          />
        </View>
        <View style={styles.HighlightItemDetailbox_S}>
          <Text
            style={styles.DetailTitle_S}
            numberOfLines={2}
            ellipsizeMode={"tail"}
          >
            Title
          </Text>
          <Text
            style={[styles.DetailTitle_S, { color: "#BABABA", marginTop: 0 }]}
          >
            K9 Around The World
          </Text>
          <Text
            style={[styles.DetailTitle_S, { color: "black", marginTop: 0 }]}
          >
            1234 View
          </Text>
        </View>
      </TouchableOpacity>
    )
  }
  latestposts = item => {
    return (
      <TouchableOpacity style={styles.Hotbox}>
        <View style={styles.HighlightItemHeaderbox}>
          <Image
            source={require("../../assets/images/example/Ski.jpg")}
            style={styles.image}
          />
        </View>
        <View style={styles.HighlightItemDetailbox}>
          <Text
            style={styles.DetailTitle}
            numberOfLines={2}
            ellipsizeMode={"tail"}
          >
            Title
          </Text>
          <Text
            style={[styles.DetailTitle, { color: "#BABABA", marginTop: 0 }]}
          >
            K9 Around The World
          </Text>
          <Text style={[styles.DetailTitle, { color: "black", marginTop: 0 }]}>
            1234 View
          </Text>
        </View>
      </TouchableOpacity>
    )
  }
}
