import React, { Component } from "react"
import {
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  Text,
  FlatList
} from "react-native"
import styles from "../styles/Destinations_Detail.style"
import { Actions } from "react-native-router-flux"

export default class DestinationsDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      Highlight_D: [{ key: 1 }, { key: 2 }, { key: 3 }],
      Tag: [
        { key: 1, name: "สายบุญ" },
        { key: 2, name: "สายกิน" },
        { key: 3, name: "สายธรรมชาติ" }
      ]
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={{ alignItems: "center" }}>
          <View style={styles.imageheaderbox}>
            <Image
              source={require("../../assets/images/example/Forbiden.jpg")}
              style={styles.image}
            />
          </View>
          <View style={styles.titletextbox}>
            <Text style={styles.titletext}>
              Forbidden City, Beijing (China)
            </Text>
          </View>
          <View style={styles.subdetailbox}>
            <View style={styles.datebox}>
              <Text>May 26 2019</Text>
            </View>
            <View style={styles.viewbox}>
              <Text>1,235 Views</Text>
            </View>
          </View>
          <View style={styles.breakline} />
          <View style={styles.detailbox}>
            <Text style={styles.detail}>
              พระราชวังต้องห้ามตั้งอยู่ใจกลางกรุงปักกิ่ง เมืองหลวงของประเทศจีน
              ซึ่งปัจจุบันได้ถูกปรับปรุง ให้เป็นพิพิธภัณฑ์พระราชวัง
              โดยครอบคลุมพื้นที่ทั้งหมด 720,000 ตารางเมตร
              ทางทิศเหนือของจัตุรัสเทียน อันเหมิน ภายในประกอบด้วยอาคารมากถึง 800
              หลัง และมีห้องทั้งสิ้น 9,999 ห้อง มีพระที่นั่ง 75 องค์
              รวมทั้งยังมีหอสมุด และห้องหับต่างๆ อีกมากมาย
              ซึ่งใช้เวลาในการก่อสร้างยาวนานถึง 14 ปี คือตั้งแต่ พ.ศ. 1949-1963
              แม้ว่าประเทศจีนจะไม่มี สถาบันพระมหากษัตริย์แล้วพระราชวังต้องห้ามก็
              ยังคงเป็นสัญลักษณ์ขอประเทศจีน
              พระราชวังต้องห้ามยังเป็นสถานที่ท่องเที่ยวที่มีชื่อเสียงที่สุดแห่งหนึ่งของโลก
              ซึ่งไม่นานมานี้
              ทางรัฐบาลจีนได้มีนโยบายจำกัดปริมาณนักท่องเที่ยวเพื่อจะอนุรักษ์สภาพของอาคารและสวนหย่อมไว้
              ยูเนสโกได้ประกาศให้พระราชวังต้องห้ามร่วมกับพระราชวังเสิ่นหยางเป็นหนึ่งในมรดกโลกในนาม
              พระราชวังหลวงแห่งราชวงศ์หมิงและราชวงศ์ชิงในปักกิ่งและเสิ่นหยาง
              เมื่อ พ.ศ. 2530 (ค.ศ. 1987) ในปี 1961 พระราชวังต้องห้าม
              ได้รับการคุ้มครองจากรัฐบาลจีนในฐานะมรดกทางวัฒนธรรมอันยิ่งใหญ่ของชาติจีน
              และได้รับการยกย่องจาก UNESCO ให้เป็นมรดกโลกในชื่อของ พระราชวังหลวง
              แห่งราชวงศ์หมิงและราชวงศ์ชิงในปักกิ่ง ภาพจาก Lovepik.com (Free
              License)
            </Text>
          </View>
          <View style={styles.breakline2} />
          <View style={styles.mapbox}>
            <Image
              source={require("../../assets/images/example/map.png")}
              style={styles.image}
            />
          </View>
          <FlatList
            data={this.state.Tag}
            renderItem={this.Tagrender}
            // numColumns={4}
            horizontal={true}
            style={styles.taglist}
          />
          <View style={styles.TarvelStyleTitlebox}>
            <View style={styles.HighlightTitlebox}>
              <View style={styles.Line} />
              <Text style={styles.TarvelStyleTitle}> RELATED </Text>
              <Text
                style={[
                  styles.TarvelStyleTitle,
                  { fontFamily: "Kanit-SemiBold", color: "#DF242B" }
                ]}
              >
                DESTINATION
              </Text>
            </View>

            <TouchableOpacity
              style={styles.morebotton}
              onPress={() => {
                Actions.Dest()
              }}
            >
              <Text style={styles.moretext}>ดูทั้งหมด</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={this.state.Highlight_D}
            renderItem={this.HighlightItem}
            horizontal={true}
            contentContainerStyle={{ padding: 10 }}
            // numColumns={4}
          />
        </ScrollView>
      </View>
    )
  }

  Tagrender = ({ item }) => {
    console.warn(item)
    return (
      <View style={styles.tagbox}>
        <Text style={styles.tagbox}>{item.name}</Text>
      </View>
    )
  }

  HighlightItem = item => {
    return (
      <TouchableOpacity style={styles.HighlightItembox} onPress={()=>{Actions.DestDe()}}>
        <View style={styles.HighlightItemHeaderbox}>
          <Image
            source={require("../../assets/images/example/Forbiden.jpg")}
            style={styles.image}
          />
        </View>
        <View style={styles.HighlightItemDetailbox}>
          <Text
            style={styles.DetailTitle}
            numberOfLines={2}
            ellipsizeMode={"tail"}
          >
            Forbidden City, Beijing (China)
          </Text>
          <Text
            style={styles.DetailText}
            numberOfLines={2}
            ellipsizeMode={"tail"}
          >
            พระราชวังต้องห้าม ตั้งอยู่ใจกลางกรุงปักกิ่ง
            เมืองหลวงของประเทศจีนซึ่งปัจจุบันได้...
          </Text>
        </View>
      </TouchableOpacity>
    )
  }
}
