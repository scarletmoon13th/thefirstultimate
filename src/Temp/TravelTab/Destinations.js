import React, { Component } from "react"
import {
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  Text,
  FlatList
} from "react-native"
import styles from "../styles/Travel_Update.style"
import { Actions } from "react-native-router-flux";
export default class Destinations extends Component {
  constructor(props) {
    super(props)
    this.state = {
      Highlight_D: [
        { key: 1 },
        { key: 2 },
        { key: 3 },
        { key: 4 },
        { key: 5 },
        { key: 6 }
      ],
      selectPage: 1,
      page: [
        { key: 1 },
        { key: 2 },
        { key: 3 },
        { key: 4 },
        { key: 5 },
        { key: 6 },
        { key: 7 },
        { key: 8 },
        { key: 9 },
        { key: 10 }
      ]
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={{ alignItems: "center" }}>
          <View style={styles.imageheader}>
            <Image
              source={require("../../assets/images/example/destinations1.jpg")}
              style={styles.image}
            />
            <Text
              style={[styles.Headertitle]}
              numberOfLines={2}
              ellipsizeMode={"tail"}
            >
              DESTINATION
            </Text>
          </View>

          <FlatList
            data={this.state.Highlight_D}
            renderItem={this.HighlightItem}

            // numColumns={4}
          />
          <View style={styles.pagingbox}>
            {this.state.page.length <= 2
              ? this.pagelessthan3()
              : this.state.selectPage == 1
              ? this.pagingFirst()
              : this.state.selectPage >= 3 &&
                this.state.selectPage < this.state.page.length - 1
              ? this.pageingMid()
              : this.pageingLast()}
          </View>
        </ScrollView>
      </View>
    )
  }
  HighlightItem = item => {
    return (
      <TouchableOpacity style={styles.Itembox} onPress={()=>{Actions.DestDe()}}>
        <View style={styles.ItemHeaderbox}>
          <Image
            source={require("../../assets/images/example/Forbiden.jpg")}
            style={styles.image}
          />
        </View>
        <View style={styles.HighlightItemDetailbox}>
          <Text
            style={styles.DetailTitle}
            numberOfLines={2}
            ellipsizeMode={"tail"}
          >
            Forbidden City, Beijing (China)
          </Text>
          <Text
            style={styles.DetailText}
            numberOfLines={2}
            ellipsizeMode={"tail"}
          >
            พระราชวังต้องห้าม ตั้งอยู่ใจกลางกรุงปักกิ่ง
            เมืองหลวงของประเทศจีนซึ่งปัจจุบันได้...
          </Text>
        </View>
      </TouchableOpacity>
    )
  }

  pagingFirst = () => {
    console.warn("enter page 1")
    return (
      <View style={styles.paginginnerbox}>
        <TouchableOpacity style={styles.pagingnumbox}>
          <Text style={styles.pagingnum}>1</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.pagingnumbox}>
          <Text style={styles.pagingnum}>2</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.pagingnumbox}>
          <Text style={styles.pagingnum}>3</Text>
        </TouchableOpacity>
        <Text style={styles.pagingnum}> ...</Text>
        <TouchableOpacity style={styles.pagingnumbox}>
          <Text style={styles.pagingnum}>{this.state.page.length}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.pagingnext}>
          <Image
            source={require("../../assets/images/arrow_forward.png")}
            style={styles.arrow}
          />
        </TouchableOpacity>
      </View>
    )
  }
  pageingMid = () => {
    return (
      <View style={styles.paginginnerbox}>
        <TouchableOpacity style={styles.pagingback} />
        <TouchableOpacity style={styles.pagingnumbox}>
          <Text style={styles.pagingnum}>1</Text>
        </TouchableOpacity>
        <Text style={styles.pagingnum}>... </Text>
        <TouchableOpacity style={styles.pagingnumbox}>
          <Text style={styles.pagingnum}>{this.state.selectPage - 1}</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.pagingnumbox}>
          <Text style={styles.pagingnum}>{this.state.selectPage}</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.pagingnumbox}>
          <Text style={styles.pagingnum}>{this.state.selectPage + 1}</Text>
        </TouchableOpacity>
        <Text style={styles.pagingnum}> ...</Text>
        <TouchableOpacity style={styles.pagingnumbox}>
          <Text style={styles.pagingnum}>{this.state.page.length}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.pagingnext} />
      </View>
    )
  }
  pageingLast = () => {
    return (
      <View style={styles.paginginnerbox}>
        <TouchableOpacity style={styles.pagingback} />
        <TouchableOpacity style={styles.pagingnumbox}>
          <Text style={styles.pagingnum}>1</Text>
        </TouchableOpacity>
        <Text style={styles.pagingnum}>... </Text>
        <TouchableOpacity style={styles.pagingnumbox}>
          <Text style={styles.pagingnum}>{this.state.page.length - 2}</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.pagingnumbox}>
          <Text style={styles.pagingnum}>{this.state.page.length - 1}</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.pagingnumbox}>
          <Text style={styles.pagingnum}>{this.state.page.length}</Text>
        </TouchableOpacity>
      </View>
    )
  }
  pagelessthan3 = () => {
    if (this.state.page.length == 1) {
      return (
        <View style={styles.paginginnerbox}>
          <TouchableOpacity style={styles.pagingnumbox}>
            <Text style={styles.pagingnum}>1</Text>
          </TouchableOpacity>
        </View>
      )
    } else if (this.state.page.length == 2) {
      return (
        <View style={styles.paginginnerbox}>
          <TouchableOpacity style={styles.pagingnumbox}>
            <Text style={styles.pagingnum}>1</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.pagingnumbox}>
            <Text style={styles.pagingnum}>2</Text>
          </TouchableOpacity>
        </View>
      )
    } else {
      ;<View style={styles.paginginnerbox}>
        <TouchableOpacity style={styles.pagingnumbox}>
          <Text style={styles.pagingnum}>1</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.pagingnumbox}>
          <Text style={styles.pagingnum}>2</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.pagingnumbox}>
          <Text style={styles.pagingnum}>3</Text>
        </TouchableOpacity>
      </View>
    }
  }
}
