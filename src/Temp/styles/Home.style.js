import { Dimensions, Platform, StyleSheet } from "react-native"
import { WHITE_COLOR ,BOTTON_COLOR,TITLE_COLOR,NEXT_COLOR,BLACK_COLOR} from "../../Constant/Color"

const { height, width } = Dimensions.get("window")
export default StyleSheet.create({
  //---------- start navigation style ----------//
  container: {
    flex: 1,
    backgroundColor: WHITE_COLOR
  },
  imageheaderbox: {
    width: width,
    height: (height / 100) * 30,
    // backgroundColor: "#874213"
    // marginTop:10
  },
  Tarvelbox: {
    width: (width / 100) * 95,
    height: (height / 100) * 25,
    // backgroundColor: "#324345",
    marginTop: 10,
    padding: 5,
    flexDirection: "row",
    // alignContent:'center',
    justifyContent: "center"
  },
  TarvelInnerbox: {
    width: "45%",
    height: "95%",
    backgroundColor: "#C4C4C4",
    alignSelf: "center",
    alignContent:'center',
    justifyContent: "center",
    borderRadius:5
    // margin:10,
  },
  TarvelPlanText:{
    fontSize: (width / 360) * 18,
    fontFamily: "Kanit-Regular",
    color:BLACK_COLOR,
    alignSelf: "center"
  },
  TarvelStyleTitlebox: {
    width: (width / 100) * 95,
    height: (height / 100) * 5,
    // backgroundColor:'#BA3898',
    marginTop: 5,
    flexDirection: "row"
  },
  TarvelStyleTitle: {
    fontSize: (width / 360) * 18,
    alignSelf: "center",
    fontFamily: "Kanit-Light",
    color:NEXT_COLOR
  },
  TarvelStyleSubTitle: {
    fontSize: (width / 360) * 12,
    alignSelf: "center",
    fontFamily: "Kanit-Light",
    marginLeft: 10
  },
  TarvelStyletItembox: {
    width: (width / 100) * 25,
    height: (height / 100) * 10,
    // backgroundColor: "#BA8",
    justifyContent: "center"
  },
  TarvelStyletItemInnerbox: {
    width: (width / 100) * 10,
    height: (width / 100) * 10,
    backgroundColor: "#C4C4C4",
    borderRadius: ((width / 100) * 10) / 2,
    alignSelf: "center"
  },
  TarvelStyletItemTitle: {
    fontSize: (width / 360) * 10,
    alignSelf: "center"
  },
  Line: {
    backgroundColor: BOTTON_COLOR,
    width: (width / 100) * 5,
    height: 2,
    alignSelf: "center"
  },
  morebotton: {
    // backgroundColor: "#987355",
    // alignSelf: "stretch",
    width: (width / 100) * 20,
    height: (height / 100) * 5,
    alignItems: "center",
    justifyContent: "center"
    // right:10
  },
  moretext: {
    fontSize: (width / 360) * 10,
    // alignSelf: "center",
    color: "black",
    fontFamily: "Kanit-Light"
  },
  HighlightTitlebox: {
    width: (width / 100) * 70,
    height: (height / 100) * 5,
    // backgroundColor:'#BA3898',
    // marginTop: 5,
    flexDirection: "row"
  },
  HighlightItembox: {
    width: (width / 100) * 80,
    height: (width / 100) * 80,
    // backgroundColor:'#BA3898',
    marginRight: 10,
    borderWidth: 0.2
  },
  HighlightItemHeaderbox: {
    width: "100%",
    height: "60%",
    backgroundColor: "#BA38"
  },
  HighlightItemDetailbox: {
    width: "100%",
    height: "40%"
    // backgroundColor:'#BA3218',
  },
  DetailTitle: {
    fontFamily: "Kanit-Regular",
    fontSize: (width / 360) * 18,
    color: TITLE_COLOR,
    margin: 10
  },
  DetailText: {
    fontFamily: "Kanit-Regular",
    fontSize: (width / 360) * 14,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10
  },
  Hotboxtitle:{
    width: (width / 100) * 95,
    height: (height / 100) * 5,
   
    marginVertical: 10,
    flexDirection: "row"
  },
  Hotbox: {
    width: (width / 100) * 100,
    height: (width / 100) * 100,
    // backgroundColor:'#BA3898',
    // marginRight:10,
    borderWidth: 0.5
  },
  HotDateBox: {
    marginLeft: 10,
    flexDirection: "row"
    // justifyContent:'center'
    // backgroundColor:'#BA3898',
  },
  HotDateText: {
    alignSelf: "center",
    fontSize: (width / 360) * 10
  },
  HotIconBox: {
    width: 15,
    height: 15,
    // backgroundColor: "#BA98"
  },
  HotPriceBox: {
    marginLeft: 10,
    marginTop: 10,
    justifyContent: "center",
    backgroundColor: BOTTON_COLOR,
    borderRadius: 5,
    alignSelf: "baseline",
    paddingVertical: 3,
    paddingHorizontal: 40
  },
  HotPriceText: {
    color: WHITE_COLOR,
    fontSize: (width / 360) * 15,
    fontFamily: "Kanit-Regular"
  },
  imageheader:{
    width:'100%',
    height: "100%"
  },
  
})
