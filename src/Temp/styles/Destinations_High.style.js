import { Dimensions, Platform, StyleSheet } from "react-native"
import {
  WHITE_COLOR,
  BOTTON_COLOR,
  TITLE_COLOR,
  NEXT_COLOR
} from "../../Constant/Color"

const { height, width } = Dimensions.get("window")
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WHITE_COLOR
  },
  imageheader: {
    width: width,
    height: (height / 100) * 15,
    // backgroundColor: "red",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 10
  },
  TarvelStyleTitlebox: {
    width: (width / 100) * 95,
    height: (height / 100) * 5,
    // backgroundColor:'#BA3898',
    marginTop: 5,
    flexDirection: "row"
  },
  TarvelStyleTitle: {
    fontSize: (width / 360) * 18,
    alignSelf: "center",
    fontFamily: "Kanit-Light",
    color:NEXT_COLOR
  },
  TarvelStyleTitle: {
    fontSize: (width / 360) * 18,
    alignSelf: "center",
    fontFamily: "Kanit-Light"
  },
  HighlightTitlebox: {
    width: (width / 100) * 70,
    height: (height / 100) * 5,
    // backgroundColor:'#BA3898',
    // marginTop: 5,
    flexDirection: "row"
  },
  Line: {
    backgroundColor: BOTTON_COLOR,
    width: (width / 100) * 5,
    height: 2,
    alignSelf: "center"
  },
  morebotton: {
    // backgroundColor: "#987355",
    // alignSelf: "stretch",
    width: (width / 100) * 20,
    height: (height / 100) * 5,
    alignItems: "center",
    justifyContent: "center"
    // right:10
  },
  moretext: {
    fontSize: (width / 360) * 10,
    // alignSelf: "center",
    color: "black",
    fontFamily: "Kanit-Light"
  },
  HighlightItembox: {
    width: (width / 100) * 95,
    height: (width / 100) * 60,
    // backgroundColor:'#BA3898',
    marginRight: 10,
    marginBottom: 10,
    borderWidth: 0.2
  },
  HighlightItemHeaderbox: {
    width: "100%",
    height: "100%",
    backgroundColor: "#776999",
    alignItems: "center",
    justifyContent: "center"
  },

  HighlightItemDetailbox: {
    width: "100%",
    height: "40%"
    // backgroundColor:'#BA3218',
  },

  Headertitle: {
    fontFamily: "Kanit-Bold",
    fontSize: (width / 360) * 20,
    color: NEXT_COLOR
  },
  title: {
    fontFamily: "Kanit-Bold",
    fontSize: (width / 360) * 18,
    color: WHITE_COLOR
  },
  image: {
    width: "100%",
    height: "100%",
    position: "absolute"
  }
})
