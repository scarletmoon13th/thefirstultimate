import { Dimensions, Platform, StyleSheet } from "react-native"
import {
  WHITE_COLOR,
  BOTTON_COLOR,
  TITLE_COLOR,
  NEXT_COLOR
} from "../../Constant/Color"

const { height, width } = Dimensions.get("window")
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WHITE_COLOR
  },
  imageheader: {
    width: width,
    height: (height / 100) * 15,
    backgroundColor: "red",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 10
  },
  Headertitle: {
    fontFamily: "Kanit-Bold",
    fontSize: (width / 360) * 20,
    color: NEXT_COLOR
  },
  pagingbox: {
    width: (width / 100) * 100,
    height: (height / 100) * 5,
    // backgroundColor: "#3898",
    marginBottom: 10,
    justifyContent: "center",
    alignItems: "flex-end"
  },
  Itembox: {
    width: (width / 100) * 100,
    height: (width / 100) * 100,
    // backgroundColor:'#BA3898',
    // marginRight: 10,
    marginBottom: 10,
    borderWidth: 0.2
  },
  ItemHeaderbox: {
    width: "100%",
    height: "60%",
    backgroundColor: "#776999",
    alignItems: "center",
    justifyContent: "center"
  },
  HighlightItemDetailbox: {
    width: "100%",
    height: "40%"
  },
  DetailTitle: {
    fontFamily: "Kanit-Regular",
    fontSize: (width / 360) * 22,
    color: TITLE_COLOR,
    margin: 10
  },
  DetailText: {
    fontFamily: "Kanit-Regular",
    fontSize: (width / 360) * 18,
    marginHorizontal: 10,
    marginBottom: 10
  },
  paginginnerbox: {
    marginRight: 10,
    flexDirection: "row"
  },
  pagingnumbox: {
    marginHorizontal: 15
  },
  pagingnum: {
    fontFamily: "Kanit-Bold",
    fontSize: (width / 360) * 22,
    alignSelf: "flex-end"
  },
  pagingnext: {
    width: (width / 100) * 10,
    height: (height / 100) * 5,
    backgroundColor: NEXT_COLOR,
    borderRadius: 5,
    alignSelf: "center",
    justifyContent:'center',
    alignItems:'center',
    marginLeft: 15,

  },
  pagingback: {
    width: (width / 100) * 10,
    height: (height / 100) * 5,
    backgroundColor: NEXT_COLOR,
    borderRadius: 5,
    alignSelf: "center",
    marginRight: 10
  },
  image: {
    width: "100%",
    height: "100%",
    position: "absolute"
  },
  arrow: {
    width: "50%",
    height: "50%",
    position: "absolute"
  },
  image:{
    width:'100%',
    height: "100%",
    position: "absolute"
  },
})
