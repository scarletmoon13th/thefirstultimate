import { Dimensions, Platform, StyleSheet } from "react-native"
import {
  WHITE_COLOR,
  BOTTON_COLOR,
  TITLE_COLOR,
  NEXT_COLOR
} from "../../Constant/Color"

const { height, width } = Dimensions.get("window")
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WHITE_COLOR
  },
  imageheader: {
    width: width,
    height: (height / 100) * 15,
    backgroundColor: "red",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 10
  },
  TarvelStyleTitlebox: {
    width: (width / 100) * 95,
    height: (height / 100) * 5,
    // backgroundColor:'#BA3898',
    marginBottom: 10,
    marginTop: 5,
    flexDirection: "row"
  },
  TarvelStyleTitle: {
    fontSize: (width / 360) * 18,
    alignSelf: "center",
    fontFamily: "Kanit-Light"
  },
  TarvelStyleTitle: {
    fontSize: (width / 360) * 18,
    alignSelf: "center",
    fontFamily: "Kanit-Light"
  },
  HighlightTitlebox: {
    width: (width / 100) * 70,
    height: (height / 100) * 5,
    // backgroundColor:'#BA3898',
    // marginTop: 5,
    flexDirection: "row"
  },
  Line: {
    backgroundColor: BOTTON_COLOR,
    width: (width / 100) * 5,
    height: 2,
    alignSelf: "center"
  },
  morebotton: {
    // backgroundColor: "#987355",
    // alignSelf: "stretch",
    width: (width / 100) * 20,
    height: (height / 100) * 5,
    alignItems: "center",
    justifyContent: "center"
    // right:10
  },
  moretext: {
    fontSize: (width / 360) * 10,
    // alignSelf: "center",
    color: "black",
    fontFamily: "Kanit-Light"
  },
  HighlightItembox: {
    width: (width / 100) * 95,
    height: (width / 100) * 60,
    // backgroundColor:'#BA3898',
    // marginRight: 10,
    marginBottom: 10,
    borderWidth: 0.2
  },
  HighlightItemHeaderbox: {
    width: "100%",
    height: "60%",
    backgroundColor: "#776999",
    alignItems: "center",
    justifyContent: "center"
  },

  HighlightItemDetailbox: {
    width: "100%",
    height: "40%"
    // backgroundColor:'#BA3218',
  },

  Headertitle: {
    fontFamily: "Kanit-Bold",
    fontSize: (width / 360) * 20,
    color: NEXT_COLOR
  },
  title: {
    fontFamily: "Kanit-Regular",
    fontSize: (width / 360) * 16,
    color: WHITE_COLOR
  },
  Hotbox: {
    width: (width / 100) * 100,
    height: (width / 100) * 100,
    // backgroundColor:'#BA3898',
    // marginRight:10,
    marginBottom: 10,
    borderWidth: 0.5
  },
  DetailTitle: {
    fontFamily: "Kanit-Regular",
    fontSize: (width / 360) * 18,
    color: TITLE_COLOR,
    margin: 10
  },
  DetailText: {
    fontFamily: "Kanit-Regular",
    fontSize: (width / 360) * 14,
    marginLeft: 10,
    marginBottom: 10
  },
  Hotbox_S: {
    width: (width / 100) * 70,
    height: (width / 100) * 40,
    // backgroundColor:'#BA3898',
    marginRight: 10,
    marginBottom: 10,
    marginLeft: 10,
    borderWidth: 0.5
  },
  DetailTitle_S: {
    fontFamily: "Kanit-Regular",
    fontSize: (width / 360) * 12,
    color: TITLE_COLOR,
    marginLeft: 5
  },
  DetailText_S: {
    fontFamily: "Kanit-Regular",
    fontSize: (width / 360) * 10
    // marginLeft: 10,
    // marginBottom: 10
  },
  HighlightItemHeaderbox_S: {
    width: "100%",
    height: "60%",
    backgroundColor: "#776999",
    alignItems: "center",
    justifyContent: "center"
  },

  HighlightItemDetailbox_S: {
    width: "100%",
    height: "40%"
    // backgroundColor:'#BA3218',
  },
  Topbox: {
    width: (width / 100) * 100,
    height: (width / 100) * 15,
    // backgroundColor: "#483",
    marginVertical: 10,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row"
  },
  leftinnerbox: {
    width: (width / 100) * 45,
    height: "80%",
    // backgroundColor: "#481113",
    marginRight: 10,
    borderWidth: 1,
    borderBottomColor: NEXT_COLOR,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center"
  },
  TextLeft: {
    fontFamily: "Kanit-Regular",
    fontSize: (width / 360) * 16,
    color: NEXT_COLOR
  },
  rightinnerbox: {
    width: (width / 100) * 45,
    height: "80%",
    backgroundColor: BOTTON_COLOR,
    borderWidth: 1,
    borderBottomColor: NEXT_COLOR,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center"
  },
  TextRight: {
    fontFamily: "Kanit-Regular",
    fontSize: (width / 360) * 16,
    color: WHITE_COLOR
  },
  image:{
    width:'100%',
    height: "100%",
    position: "absolute"
  }
})
