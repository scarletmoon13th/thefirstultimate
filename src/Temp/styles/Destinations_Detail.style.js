import { Dimensions, Platform, StyleSheet } from "react-native"
import {
  WHITE_COLOR,
  BOTTON_COLOR,
  TITLE_COLOR,
  NEXT_COLOR,
  TEXT_COLOR
} from "../../Constant/Color"

const { height, width } = Dimensions.get("window")

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WHITE_COLOR
  },
  imageheaderbox: {
    width: width,
    height: (height / 100) * 30
  },
  image: {
    width: "100%",
    height: "100%"
  },
  titletextbox: {
    width: (width / 100) * 90,
    // height: (height / 100) * 10,
    // backgroundColor:'#1989',
    justifyContent: "center",
    marginTop: 20
    // marginBottom:5
  },
  titletext: {
    fontFamily: "Kanit-Regular",
    fontSize: (width / 360) * 18,
    color: TITLE_COLOR
    // margin: 10
  },
  subdetailbox: {
    width: (width / 100) * 90,
    // backgroundColor:'#763474',
    height: (height / 100) * 6,
    flexDirection: "row",
    marginLeft: 10
    // justifyContent:'center',
  },
  datebox: {
    borderRightWidth: 1,
    paddingRight: 5,
    alignSelf: "center"
  },
  viewbox: {
    paddingLeft: 5,
    alignSelf: "center"
  },
  breakline: {
    width: (width / 100) * 80,
    borderTopWidth: 1,
    marginVertical: 10
  },
  detailbox: {
    width: (width / 100) * 90,
    // backgroundColor:'#763474',
    marginBottom: 20
  },
  detail: {
    color: TEXT_COLOR,
    fontFamily: "Kanit-Regular",
    fontSize: (width / 360) * 16,
    textAlign: "justify"
  },
  breakline2: {
    width: (width / 100) * 100,
    borderTopWidth: 1,
    marginVertical: 10
  },
  mapbox: {
    width: (width / 100) * 80,
    height: (width / 100) * 80,
    backgroundColor: "#989",
    marginVertical: 10
  },
  taglist:{
    width: (width / 100) * 90,
  },
  tagbox: {
    // padding: 5,
    backgroundColor: TEXT_COLOR,
    justifyContent: "center",
    alignItems: "center",
    borderRadius:5,
    margin:10
  },
  tagname:{
    fontFamily: "Kanit-Regular",
    fontSize: (width / 360) * 14,
    color: TITLE_COLOR,
    
  },
  TarvelStyleTitlebox: {
    width: (width / 100) * 95,
    height: (height / 100) * 5,
    // backgroundColor:'#BA3898',
    marginTop: 5,
    flexDirection: "row"
  },
  TarvelStyleTitle: {
    fontSize: (width / 360) * 18,
    alignSelf: "center",
    fontFamily: "Kanit-Light",
    color:NEXT_COLOR
  },
  TarvelStyleSubTitle: {
    fontSize: (width / 360) * 12,
    alignSelf: "center",
    fontFamily: "Kanit-Light",
    marginLeft: 10
  },
  TarvelStyleTitlebox: {
    width: (width / 100) * 95,
    height: (height / 100) * 5,
    // backgroundColor:'#BA3898',
    marginTop: 5,
    flexDirection: "row"
  },
  HighlightTitlebox: {
    width: (width / 100) * 70,
    height: (height / 100) * 5,
    // backgroundColor:'#BA3898',
    // marginTop: 5,
    flexDirection: "row"
  },
  Line: {
    backgroundColor: BOTTON_COLOR,
    width: (width / 100) * 5,
    height: 2,
    alignSelf: "center"
  },TarvelStyleTitle: {
    fontSize: (width / 360) * 18,
    alignSelf: "center",
    fontFamily: "Kanit-Light",
    color:NEXT_COLOR
  },morebotton: {
    // backgroundColor: "#987355",
    // alignSelf: "stretch",
    width: (width / 100) * 20,
    height: (height / 100) * 5,
    alignItems: "center",
    justifyContent: "center"
    // right:10
  }, moretext: {
    fontSize: (width / 360) * 10,
    // alignSelf: "center",
    color: "black",
    fontFamily: "Kanit-Light"
  },
  HighlightItembox: {
    width: (width / 100) * 80,
    height: (width / 100) * 80,
    // backgroundColor:'#BA3898',
    marginRight: 10,
    borderWidth: 0.2
  },
  HighlightItemHeaderbox: {
    width: "100%",
    height: "60%",
    backgroundColor: "#BA38"
  },HighlightItemDetailbox: {
    width: "100%",
    height: "40%"
    // backgroundColor:'#BA3218',
  },
  DetailTitle: {
    fontFamily: "Kanit-Regular",
    fontSize: (width / 360) * 18,
    color: TITLE_COLOR,
    margin: 10
  },
  DetailText: {
    fontFamily: "Kanit-Regular",
    fontSize: (width / 360) * 14,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10
  },
})
