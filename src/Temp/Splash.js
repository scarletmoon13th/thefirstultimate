import React, { Component } from "react"
import { View, Image, Platform, Dimensions } from "react-native"
import { Actions } from "react-native-router-flux"

const { width, height } = Dimensions.get("window")

export default class splash extends Component {
  componentDidMount() {
    setTimeout(() => {
      Actions.replace("Home")
    }, 1500)
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: "white",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Image
          source={require("../assets/images/Logo_app.jpg")}
          style={{ width: (width / 100) * 60, height: (height / 100) * 40 }}
        />
      </View>
    )
  }
}
