import React, { Component } from "react"
import {
  View,
  Image,
  Platform,
  ScrollView,
  TouchableOpacity,
  Text,
  FlatList
} from "react-native"
import styles from "./styles/Home.style"
import { Actions } from "react-native-router-flux"
import { ICONS } from "jest-util/build/specialChars";

export default class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      Tarvelstyle: [
        { key: 1 },
        { key: 2 },
        { key: 3 },
        { key: 4 },
        { key: 5 },
        { key: 6 },
        { key: 7 },
        { key: 8 },
        { key: 9 },
        { key: 10 },
        { key: 11 },
        { key: 12 }
      ],
      Highlight_D: [{ key: 1 }, { key: 2 }, { key: 3 }],
      Travel_update: [{ key: 1 }, { key: 2 }, { key: 3 }]
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          style={styles.body}
          contentContainerStyle={{ alignItems: "center" }}
        >
          {/* HEADER */}
          <TouchableOpacity style={styles.imageheaderbox}>
            <Image
              source={require("../assets/images/example/airport.jpg")}
              style={styles.imageheader}
            />
          </TouchableOpacity>

          <View style={styles.Tarvelbox}>
            <TouchableOpacity
              style={[styles.TarvelInnerbox, { marginRight: 10 }]}  onPress={()=>{Actions.TravP()}}
            >
              <Text style={styles.TarvelPlanText}>Travel Plan</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.TarvelInnerbox}  onPress={()=>{Actions.TravS()}}>
              <Text style={styles.TarvelPlanText}>Travel Story</Text>
            </TouchableOpacity>
          </View>
          {/* Title TarvelStyle */}
          <View style={styles.TarvelStyleTitlebox}>
            <Text style={styles.TarvelStyleTitle}>TRAVEL </Text>
            <Text
              style={[
                styles.TarvelStyleTitle,
                { fontFamily: "Kanit-SemiBold", color: "#DF242B" }
              ]}
            >
              STYLE
            </Text>
          </View>
          <View style={[styles.TarvelStyleTitlebox, { marginTop: 0 }]}>
            <Text style={[styles.TarvelStyleSubTitle]}>
              ถ้าคุณเป็นคนชอบท่องเที่ยว ลองดูสไตล์การเที่ยวที่เหมาะกับคุณ
            </Text>
          </View>
          <FlatList
            data={this.state.Tarvelstyle}
            renderItem={this.TarvelStyle}
            numColumns={4}
          />
          <View style={styles.TarvelStyleTitlebox}>
            <View style={styles.HighlightTitlebox}>
              <View style={styles.Line} />
              <Text style={styles.TarvelStyleTitle}> HIGHLIGHT </Text>
              <Text
                style={[
                  styles.TarvelStyleTitle,
                  { fontFamily: "Kanit-SemiBold", color: "#DF242B" }
                ]}
              >
                DESTINATION
              </Text>
            </View>

            <TouchableOpacity
              style={styles.morebotton}
              onPress={() => {
                Actions.DestH()
              }}
            >
              <Text style={styles.moretext}>ดูทั้งหมด</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={this.state.Highlight_D}
            renderItem={this.HighlightItem}
            horizontal={true}
            contentContainerStyle={{ padding: 10 }}
            // numColumns={4}
          />
          <View style={styles.TarvelStyleTitlebox}>
            <View style={styles.HighlightTitlebox}>
              <View style={styles.Line} />
              <Text style={styles.TarvelStyleTitle}> TRAVEL </Text>
              <Text
                style={[
                  styles.TarvelStyleTitle,
                  { fontFamily: "Kanit-SemiBold", color: "#DF242B" }
                ]}
              >
                UPDATE
              </Text>
            </View>

            <TouchableOpacity
              style={styles.morebotton}
              onPress={() => {
                Actions.TravU()
              }}
            >
              <Text style={styles.moretext}>ดูทั้งหมด</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={this.state.Travel_update}
            renderItem={this.HighlightItem}
            horizontal={true}
            contentContainerStyle={{ padding: 10 }}
            // numColumns={4}
          />
          <View style={styles.Hotboxtitle}>
            <View style={styles.HighlightTitlebox}>
              <View style={styles.Line} />
              <Text style={styles.TarvelStyleTitle}> Hot</Text>
              <Text
                style={[
                  styles.TarvelStyleTitle,
                  { fontFamily: "Kanit-SemiBold", color: "#DF242B" }
                ]}
              >
                Video
              </Text>
            </View>
          </View>
          <TouchableOpacity style={styles.Hotbox}>
            <View style={styles.HighlightItemHeaderbox}>
              <Image
                source={require("../assets/images/example/serie.jpg")}
                style={styles.imageheader}
              />
            </View>
            <View style={styles.HighlightItemDetailbox}>
              <Text
                style={styles.DetailTitle}
                numberOfLines={2}
                ellipsizeMode={"tail"}
              >
                {"The First Ultimate เที่ยวสุดโลก EP.52 South Korea (ตอน 1)"}
              </Text>
              <Text
                style={styles.DetailText}
                numberOfLines={2}
                ellipsizeMode={"tail"}
              >
                {
                  "เต๋าจะพาทุกคน ไปพบกับเส้นทางใหม่ ของการท่องเที่ยวที่ประเทศเกาหลีใต้ จะลัดเลาะลงไปทางโซนภาคใต้เท่า..."
                }
              </Text>
            </View>
          </TouchableOpacity>
          <View style={styles.Hotboxtitle}>
            <View style={styles.HighlightTitlebox}>
              <View style={styles.Line} />
              <Text style={styles.TarvelStyleTitle}> Hot</Text>
              <Text
                style={[
                  styles.TarvelStyleTitle,
                  { fontFamily: "Kanit-SemiBold", color: "#DF242B" }
                ]}
              >
                Trip
              </Text>
            </View>
          </View>
          <TouchableOpacity style={styles.Hotbox}>
            <View style={styles.HighlightItemHeaderbox}>
              <Image
                source={require("../assets/images/example/tour.jpg")}
                style={styles.imageheader}
              />
            </View>
            <View style={styles.HighlightItemDetailbox}>
              <Text
                style={styles.DetailTitle}
                numberOfLines={2}
                ellipsizeMode={"tail"}
              >
                ทัวร์เกาหลีใต้ 7 วัน 5 คืน
              </Text>
              <View style={styles.HotDateBox}>
                <View style={styles.HotIconBox} >
                  <Image
                  source={require("../assets/images/event_note.png")}
                  style={styles.imageheader}
                />
                </View>
                <Text style={styles.HotDateText}> 4/12/2019 - 10/12/2019</Text>
              </View>
              <View style={styles.HotPriceBox}>
                <Text style={styles.HotPriceText}>47,000</Text>
              </View>
            </View>
          </TouchableOpacity>
          <View style={styles.Hotboxtitle}>
            <View style={styles.HighlightTitlebox}>
              <View style={styles.Line} />
              <Text style={styles.TarvelStyleTitle}> Hot</Text>
              <Text
                style={[
                  styles.TarvelStyleTitle,
                  { fontFamily: "Kanit-SemiBold", color: "#DF242B" }
                ]}
              >
                Topic
              </Text>
            </View>
          </View>
          <TouchableOpacity style={styles.Hotbox}>
            <View style={styles.HighlightItemHeaderbox}>
              <Image
                source={require("../assets/images/example/Ski.jpg")}
                style={styles.imageheader}
              />
            </View>
            <View style={styles.HighlightItemDetailbox}>
              <Text
                style={styles.DetailTitle}
                numberOfLines={2}
                ellipsizeMode={"tail"}
              >
                Title
              </Text>
              <Text
                style={[styles.DetailTitle, { color: "#BABABA", marginTop: 0 }]}
              >
                K9 Around The World
              </Text>
              <Text
                style={[styles.DetailTitle, { color: "black", marginTop: 0 }]}
              >
                1234 View
              </Text>
            </View>
          </TouchableOpacity>
        </ScrollView>
      </View>
    )
  }

  TarvelStyle = item => {
    return (
      <TouchableOpacity style={styles.TarvelStyletItembox}>
        <View style={styles.TarvelStyletItemInnerbox} />
        <Text style={styles.TarvelStyletItemTitle}>XXXXXX</Text>
      </TouchableOpacity>
    )
  }
  HighlightItem = item => {
    return (
      <TouchableOpacity style={styles.HighlightItembox} onPress={()=>{Actions.DestDe()}}>
        <View style={styles.HighlightItemHeaderbox}>
          <Image
            source={require("../assets/images/example/Forbiden.jpg")}
            style={styles.imageheader}
          />
        </View>
        <View style={styles.HighlightItemDetailbox}>
          <Text
            style={styles.DetailTitle}
            numberOfLines={2}
            ellipsizeMode={"tail"}
          >
            Forbidden City, Beijing (China)
          </Text>
          <Text
            style={styles.DetailText}
            numberOfLines={2}
            ellipsizeMode={"tail"}
          >
            พระราชวังต้องห้าม ตั้งอยู่ใจกลางกรุงปักกิ่ง
            เมืองหลวงของประเทศจีนซึ่งปัจจุบันได้...
          </Text>
        </View>
      </TouchableOpacity>
    )
  }
}
