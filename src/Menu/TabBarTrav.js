import React, { Component } from "react"
import {
  TouchableOpacity,
  StyleSheet,
  View,
  Dimensions,
  Text
} from "react-native"
import { Actions } from "react-native-router-flux"

import { WHITE_COLOR, BOTTON_COLOR } from "../Constant/Color"

const { height, width } = Dimensions.get("window")

export default class TabBarTrav extends Component {
  constructor(props) {
    super(props)
    this.state = {
      vale: 0,
      pageNow: null
    }
  }
  componentDidMount() {}

  checkpage() {
    let pageNow = Actions.currentScene
    if (pageNow != this.state.pageNow) {
      this.setState({ pageNow: pageNow })
    }
  }

  ToPage(option) {
    // console.warn(option)

    if (option == "DestH") {
      Actions.DestH()
      this.checkpage()
    } else if (option == "TravU") {
      Actions.TravU()
      this.checkpage()
    } else if (option == "TravP") {
      Actions.TravP()
      this.checkpage()
    } else if (option == "TravS") {
      Actions.TravS()
      this.checkpage()
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.comlum}
          onPress={() => {
            this.ToPage("DestH")
          }}
        >
          <View style={[styles.titlebox]}>
            <View style={styles.line}>
              <Text style={styles.titleTab}>Destination</Text>
            </View>
          </View>

          {/*(this.state.pageNow == "DestH" ||
            this.state.pageNow == "Dest" ||
            this.state.pageNow == "DestDe") && (
            <View style={styles.currentpage} />
            )*/}
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.comlum}
          onPress={() => {
            this.ToPage("TravU")
          }}
        >
          <View style={styles.line}>
            <Text style={styles.titleTab}>Travel Update</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.comlum}
          onPress={() => {
            this.ToPage("TravP")
          }}
        >
          <View style={styles.line}>
            <Text style={styles.titleTab}>Travel Plan</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.comlum}
          onPress={() => {
            this.ToPage("TravS")
          }}
        >
          <Text style={styles.titleTab}>Travel Story</Text>
        </TouchableOpacity>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    width: width,
    height: (height / 100) * 8,
    backgroundColor: "#0C3274",
    flexDirection: "row"
  },
  comlum: {
    width: "25%",
    height: "100%",
    // backgroundColor: "#166111",
    // padding: 15,
    alignItems: "center",
    justifyContent: "center"
  },
  titlebox: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center"
  },
  titleTab: {
    fontSize: (width / 360) * 12,
    // alignSelf: "center",
    fontFamily: "Kanit-Regular",

    color: WHITE_COLOR
  },
  line: {
    width: "100%",
    borderRightWidth: 2,
    borderColor: WHITE_COLOR,
    alignItems: "center",
    justifyContent: "center"
  },
  currentpage: {
    width: "100%",
    height: "7%",
    backgroundColor: BOTTON_COLOR
  }
})
