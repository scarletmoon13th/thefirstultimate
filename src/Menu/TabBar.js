import React, { Component } from "react"
import { Platfor, StyleSheet } from "react-native"
import { View, Image, TouchableOpacity, Dimensions } from "react-native"

import { Actions } from "react-native-router-flux"

const { height, width } = Dimensions.get("window")

export default class TabBar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      vale: 0
    }
  }
  componentDidMount() {}

  ToPage = page => {
    console.warn(Actions.currentScene)
    if (page == "home") {
      Actions.Home()
    } else if (page == "dest") {
      Actions.DestH()
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.comlum}
          onPress={() => {
            this.ToPage("home")
          }}
        >
          <Image
            source={require("../assets/images/home_unfill.png")}
            style={styles.image}
            resizeMode={"contain"}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.comlum}
          onPress={() => this.ToPage("dest")}
        >
          <Image
            source={require("../assets/images/rate_review_unfill.png")}
            style={styles.image}
            resizeMode={"contain"}
          />
        </TouchableOpacity>
        <TouchableOpacity style={styles.comlum}>
          <Image
            source={require("../assets/images/video_unfill.png")}
            style={styles.image}
            resizeMode={"contain"}
          />
        </TouchableOpacity>
        <TouchableOpacity style={styles.comlum}>
          <Image
            source={require("../assets/images/outlined_unfill.png")}
            style={styles.image}
            resizeMode={"contain"}
          />
        </TouchableOpacity>
        <TouchableOpacity style={styles.comlum}>
          <Image
            source={require("../assets/images/account_circle_unfill.png")}
            style={styles.image}
            resizeMode={"contain"}
          />
        </TouchableOpacity>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    width: width,
    height: (height / 100) * 8,
    // backgroundColor: "#166",
    borderTopWidth: 0.5,
    flexDirection: "row"
  },
  comlum: {
    width: "20%",
    height: "100%",
    // backgroundColor: "#166111",
    padding: 15
  },
  image: {
    width: "100%",
    height: "100%"
  }
})
