import React, { Component } from "react"
import { Platfor, StyleSheet } from "react-native"
import {
  View,
  Image,
  Platform,
  ScrollView,
  TouchableOpacity,
  Text,
  FlatList,
  Animated,
  Dimensions
} from "react-native"

import { WHITE_COLOR, TITLE_COLOR } from "../Constant/Color"

const { height, width } = Dimensions.get("window")

export default class Menu extends Component {
  constructor(props) {
    super(props)
    this.state = {
      vale: 0
    }
  }
  componentDidMount() {}
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.imagebox}>
          <Image
            source={require("../assets/images/Logo_app.jpg")}
            style={styles.image}
          />
        </View>
        <TouchableOpacity style={styles.searchbox}>
          <Text style={styles.searchtext}>YOUR DESTINATIONS</Text>
          <View style={styles.searchimagebox}>
            <Image
              source={require("../assets/images/search_24px.png")}
              style={styles.image}
            />
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    width: width,
    height: (height / 100) * 8,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    // justifyContent: "center",
    backgroundColor: WHITE_COLOR,
    flexDirection: "row"
  },
  imagebox: {
    width: (width / 100) * 10,
    height: (width / 100) * 10,
    marginLeft: 10,
    alignSelf: "center"
  },
  image: {
    width: "100%",
    height: "100%"
  },
  searchbox: {
    // backgroundColor: "#3999",
    width: (width / 100) * 85,
    flexDirection: "row",
    justifyContent: "flex-end"
  },
  searchtext: {
    fontFamily: "Kanit-Regular",
    fontSize: (width / 360) * 14,
    color: TITLE_COLOR,
    alignSelf: "center"
  },
  searchimagebox: {
    width: (width / 100) * 5,
    height: (width / 100) * 5,
    marginLeft: 10,
    alignSelf: "center"
  }
})
