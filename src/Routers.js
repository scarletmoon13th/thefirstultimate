import React, { Component } from "react"
import { Platfor, StyleSheet } from "react-native"
import {
  Scene,
  Router,
  Actions,
  ActionConst,
  Overlay,
  Tabs,
  Modal,
  Stack
} from "react-native-router-flux"

//menu Component
import Menu from "./Menu/Menu"
import TabBar from "./Menu/TabBar"
import TabBarTrav from "./Menu/TabBarTrav"

//Page
import Splash from "./Temp/Splash"
import Home from "./Temp/Home"
import DestinationsHightlight from "./Temp/TravelTab/Destinations_Hightlight"
import TravelUpdate from './Temp/TravelTab/Tarvel_Update'
import TarvelPlanhot from './Temp/TravelTab/Tarvel_Plan_hot'
import TarvelStoryhot from './Temp/TravelTab/Travel_Story_hot'
import Destinations from './Temp/TravelTab/Destinations'
import DestinationsDetail from './Temp/TravelTab/Destinations_Detail'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center"
  },
  scene: {
    backgroundColor: "#F5FCFF",
    shadowOpacity: 1,
    shadowRadius: 3
  },
  tabBarStyle: {
    backgroundColor: "#eee"
  },
  tabBarSelectedItemStyle: {
    backgroundColor: "#ddd"
  }
})

const stateHandler = (prevState, newState, action) => {
  console.log("onStateChange: ACTION:", action)
}

export default class Routers extends Component {
  render() {
    return (
      <Router onStateChange={stateHandler} sceneStyle={styles.scene}>
        <Overlay key="overlay">
          <Modal key="modal" hideNavBar>
            <Stack key="root" hideNavBar>
              <Scene
                key="Splash"
                component={Splash}
                title="Splash"
                initial
                type={ActionConst.RESET}
              />
              <Stack key="customNavBar" hideNavBar>
                <Scene navBar={Menu}>
                  <Scene
                    hideNavBar
                    tabBarComponent={TabBar}
                    tabs={true}
                    tabBarPosition={"bottom"}
                  >
                    <Scene key="Home" title="Home" component={Home} />
                    <Stack key="tabTarv" >
                      <Scene
                        tabBarComponent={TabBarTrav}
                        tabs={true}
                        tabBarPosition={"top"}
                      >
                      <Stack key="Desttab" >
                      <Scene key="DestH" component={DestinationsHightlight} hideNavBar/>
                      <Scene key="Dest" component={Destinations} hideNavBar/>
                      <Scene key="DestDe" component={DestinationsDetail} hideNavBar/>
                      </Stack>
                      
                      
                      <Scene key="TravU" component={TravelUpdate} hideNavBar/>
                      <Scene key="TravP" component={TarvelPlanhot} hideNavBar/>
                      <Scene key="TravS" component={TarvelStoryhot} hideNavBar/>
                      </Scene>
                    </Stack>
                  </Scene>
                </Scene>
              </Stack>
            </Stack>
          </Modal>
        </Overlay>
      </Router>
    )
  }
}
